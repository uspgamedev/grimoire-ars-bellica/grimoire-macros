extern crate grimoire_common;
extern crate proc_macro;
extern crate toml;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn;

#[proc_macro_derive(IDable)]
pub fn idable_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_idable_macro(&ast)
}

fn impl_idable_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let get_id_definition = quote! {
        fn get_id(&self) -> ID {
            self.id
        }
    };
    let gen = quote! {
        impl IDable for #name {
            #get_id_definition
        }
    };
    gen.into()
}

#[proc_macro_derive(Component)]
pub fn component_macro_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_component_macro(&ast)
}

fn impl_component_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl Component for #name {}
    };
    gen.into()
}

